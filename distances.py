"""
Module providing distances array
"""
from typing import List
import numpy as np
from geopy import distance
from town_generator import City


def get_distances(lst_of_cities: List[City]) -> np.ndarray:
    """
    Function to calculate and provide array of distances between provided cities.
    :param lst_of_cities: List containing City type objects
    :return:
    """
    cities_dist = np.ndarray(shape=(len(lst_of_cities), len(lst_of_cities)))
    for i in range(len(lst_of_cities)):
        for j in range(len(lst_of_cities)):
            cities_dist[i-1, j-1] = distance.distance((lst_of_cities[i].latitude,
                                                       lst_of_cities[i].longitude),
                                                      (lst_of_cities[j].latitude,
                                                       lst_of_cities[j].longitude)).kilometers
    return cities_dist
