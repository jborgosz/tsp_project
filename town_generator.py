from typing import List
import tqdm
import faker
from geopy.geocoders import Nominatim
from dataclasses import dataclass


@dataclass
class City:
    name: str
    latitude: float
    longitude: float


def generate_cities(number: int) -> List[City]:
    fake = faker.Faker('pl_PL')
    faker.Faker.seed(0)
    cities = [fake.city() for _ in range(number)]

    geo_locator = Nominatim(user_agent="NO_NAME")
    coords = [geo_locator.geocode(city) for city in tqdm.tqdm(cities)]

    return [City(name=city, latitude=coords.latitude, longitude=coords.longitude)
            for city, coords
            in zip(cities, coords)]
