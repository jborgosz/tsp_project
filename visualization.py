from typing import List
from town_generator import City
import gmplot


def visualize(cities: List[City]):

    name_lst = []
    latitude_lst = []
    longitude_lst = []

    for city in cities:
        name_lst.append(city.name)
        latitude_lst.append(city.latitude)
        longitude_lst.append(city.longitude)

    gm_map = gmplot.GoogleMapPlotter(51.981453, 19.354105, 7)
    gm_map.scatter(latitude_lst, longitude_lst, color='red', size=50, marker=True, label=name_lst)
    gm_map.plot(latitude_lst, longitude_lst, edge_width=3.0, color='black')
    gm_map.draw('map.html')

    return name_lst, latitude_lst, longitude_lst


